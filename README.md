# Jekyll Theme Gem for Presentations

**If you simply want to create a presentation within the VGIscience domain using this theme, just go ahead and fork [the Presentation Template](https://gitlab.vgiscience.de/templates/presentation) and follow the instructions in that project's `README.md`. It already utilizes this theme gem.**

The following is only of interest for the integration in remote Jekyll instances.

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "vgiscience-presentation-theme", :git => 'https://gitlab.vgiscience.de/design/themes/presentations/vgiscience.git'
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: vgiscience-presentation-theme
```

And then execute:

```shell
bundle update
```
